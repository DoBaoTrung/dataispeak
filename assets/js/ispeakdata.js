var max_point = 40;
var bg_color;

if (potential_points >= 35){
    bg_color = '#00fffc';
}
else if (potential_points >= 25 && potential_points < 35){
    bg_color = '#3ec556'
}
else if (potential_points >= 15 && potential_points < 25){
    bg_color = '#ffd400'
}
else {
    bg_color = '#ff2a00'
}

var deliveredData = {
    labels: [
        "Điểm tiềm năng khách hàng",
        "Còn thiếu"
    ],
    datasets: [
        {
            data: [potential_points, max_point - potential_points],
            backgroundColor: [
                bg_color,
                "#eee"
            ],
            borderWidth: [
                0, 0
            ]
        }
    ]
};

var deliveredOpt = {
    cutoutPercentage: 88,
    animation: {
        animationRotate: true,
        duration: 2000
    },
    legend: {
        display: false
    },
    tooltips: {
        enabled: true
    },
};

var chart = new Chart($('#chart'), {
    type: 'doughnut',
    data: deliveredData,
    options: deliveredOpt
});