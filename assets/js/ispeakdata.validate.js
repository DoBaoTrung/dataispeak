$("#dataispeak-form").validate({
	rules: {
	    "bonus[school_name]": {
	    	'min': 0,
	    	'max': 3
		},
		"bonus[address]": {
	    	'min': 0,
	    	'max': 2
		},
		"bonus[education_center_name]": {
	    	'min': 0,
	    	'max': 3
		},
		"bonus[parents_job]": {
	    	'min': 0,
	    	'max': 3
		}
    }, 
	messages: {
		"bonus[school_name]": {
			'min': 'Ít nhất {0}đ',
			'max': 'Chỉ được nhập tối đa {0}đ'
		},
		"bonus[address]": {
			'min': 'Ít nhất {0}đ',
			'max': 'Chỉ được nhập tối đa {0}đ'
		},
		"bonus[education_center_name]": {
			'min': 'Ít nhất {0}đ',
			'max': 'Chỉ được nhập tối đa {0}đ'
		},
		"bonus[parents_job]": {
			'min': 'Ít nhất {0}đ',
			'max': 'Chỉ được nhập tối đa {0}đ'
		}
	},
  	submitHandler: function(form) {
  		if ($(this).valid())
    		form.submit();
  	}
});