jQuery(document).ready(function($) {
    if (typeof $.timepicker != 'undefined') {
        $.timepicker.regional['vi'] = {
            closeText: "Đóng",
            prevText: "&#x3C;Trước",
            nextText: "Tiếp&#x3E;",
            currentText: "Hôm nay",
            monthNames: ["Tháng Một", "Tháng Hai", "Tháng Ba", "Tháng Tư", "Tháng Năm", "Tháng Sáu",
                "Tháng Bảy", "Tháng Tám", "Tháng Chín", "Tháng Mười", "Tháng Mười Một", "Tháng Mười Hai"
            ],
            monthNamesShort: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6",
                "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"
            ],
            dayNames: ["Chủ Nhật", "Thứ Hai", "Thứ Ba", "Thứ Tư", "Thứ Năm", "Thứ Sáu", "Thứ Bảy"],
            dayNamesShort: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            weekHeader: "Tu",
            dateFormat: "dd/mm/yy",
            firstDay: 0,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",

            timeOnlyTitle: 'Chọn giờ',
            timeText: 'Thời gian',
            hourText: 'Giờ',
            minuteText: 'Phút',
            secondText: 'Giây',
            millisecText: 'Mili giây',
            microsecText: 'Micrô giây',
            timezoneText: 'Múi giờ',
            currentText: 'Hiện thời',
            closeText: 'Đóng',
            timeFormat: 'HH:mm',
            timeSuffix: '',
            amNames: ['SA', 'S'],
            pmNames: ['CH', 'C'],
            isRTL: false
        };

        $.timepicker.setDefaults($.timepicker.regional['vi']);

        var dt = new Date();
        var nextYear = dt.getFullYear() + 1;
        $('.datetime').datetimepicker({
            dateFormat: "dd/mm/yy",
            timeInput: true,
            changeMonth: true,
            changeYear: true,
            yearRange: '2016:' + nextYear
        });
    }

    $('.mask').click(function(event) {
        var target = $(this).data('target');
        var value = $(this).data('target-value');
        var type = $(this).data('target-type');
        switch (type){
        	case 'select':
        		$(target).find('option[value=' + value + ']').prop('selected','selected');
        		break;
        	case 'radio':
	        	$(target).filter(function() {return $(this).val() == value;}).prop('checked','checked');
        		break;
        	default:
        		alert('You didn\'t define target-type');
        		break;
        }
        $(this).removeClass('btn-default').addClass('btn-info');
		$(this).siblings('.mask').removeClass('btn-info').addClass('btn-default');
    });
});
