jQuery(document).ready(function($) {
	/*
	 * VARIABLES
	 */
	var messageCount=0,max_message=3;
	var delay = 5000;

	/** END VARIABLES */
	if (isSticky){
		delay = 1000000;
	}

	if (typeof $().tooltip === "function") { 
	    $('[data-toggle="tooltip"]').tooltip(); 
	}

	/**
	 * Hiện thông điệp đến người dùng
	 * @param  {[String]} text  [Nội dung thông điệp]
	 * @param  {String} type  [Thể loại thông báo (error info....)]
	 * @param  {String} title [Tiếu đề]
	 * @param  {String} icon  [Biểu tượng]
	 * @param  {Number} delay [Thời gian hiện]
	 * @return {[type]}       [description]
	 */
	function showMessage(text, type="success",title="Thông báo",icon="glyphicon-ok") {
		if (messageCount<max_message){
			messageCount++;
		}
		else{
			messageCount=1;
			PNotify.removeAll();
		}
        var notice = new PNotify({
            title: title,
           	text: text,
            type: type,
            icon: 'glyphicon ' + icon,
            delay: delay,
        })
    }

	if (success){
		showMessage(success, "success", "Thành công!");
	}
	if (notice){
		showMessage(notice, "notice", "Thông báo");
	}
	if (error){
		showMessage(error, "error", "Có lỗi xảy ra!","glyphicon-error");
	}

});