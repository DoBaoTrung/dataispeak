-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2017 at 09:09 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dataispeak`
--

-- --------------------------------------------------------

--
-- Table structure for table `khachhang`
--

CREATE TABLE `khachhang` (
  `id` int(11) UNSIGNED NOT NULL,
  `hoten` varchar(255) NOT NULL,
  `ten` varchar(255) DEFAULT NULL,
  `gioitinh` int(11) DEFAULT NULL,
  `tuoi` int(11) NOT NULL,
  `diachi` varchar(255) DEFAULT NULL,
  `tentruong` varchar(255) DEFAULT NULL,
  `kquahoctap` varchar(255) DEFAULT NULL,
  `diemtiemnang` int(10) UNSIGNED DEFAULT NULL,
  `nguoinhap` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `trangthai` int(11) DEFAULT '0',
  `diemtudanhgia` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `khachhang`
--

INSERT INTO `khachhang` (`id`, `hoten`, `ten`, `gioitinh`, `tuoi`, `diachi`, `tentruong`, `kquahoctap`, `diemtiemnang`, `nguoinhap`, `created_at`, `updated_at`, `trangthai`, `diemtudanhgia`) VALUES
(1, 'Junior', '', 1, 13, 'asd', '', '1', 33, 1, 1498385847, NULL, 1, '{\"diachi\":\"2\",\"tentruong\":\"3\",\"tentrungtam\":\"3\",\"noicongtaccuabome\":\"3\"}');

-- --------------------------------------------------------

--
-- Table structure for table `khachhang_statuses`
--

CREATE TABLE `khachhang_statuses` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `class` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `khachhang_statuses`
--

INSERT INTO `khachhang_statuses` (`id`, `name`, `class`, `created_at`, `updated_at`) VALUES
(1, 'Pending', 'pending', 1498305096, 1498308358),
(2, 'Có tài khoản', 'co-tai-khoan', 1498308388, 1498308388),
(3, 'Học viên', 'hoc-vien', 1498308396, 1498308396),
(4, 'Đã học thử', 'da-hoc-thu', 1498308405, 1498308405);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `type` varchar(25) NOT NULL,
  `name` varchar(50) NOT NULL,
  `migration` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`type`, `name`, `migration`) VALUES
('app', 'default', '001_create_users'),
('app', 'default', '002_create_khachhang'),
('app', 'default', '003_create_ttin_hoctienganh'),
('app', 'default', '004_create_ttin_trangbi'),
('app', 'default', '005_create_thongtinthem'),
('app', 'default', '006_create_khachhang_statuses');

-- --------------------------------------------------------

--
-- Table structure for table `thongtinthem`
--

CREATE TABLE `thongtinthem` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_khachhang` int(11) NOT NULL,
  `bomehoccung` int(11) DEFAULT NULL,
  `noicongtaccuabome` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `thongtinthem`
--

INSERT INTO `thongtinthem` (`id`, `id_khachhang`, `bomehoccung`, `noicongtaccuabome`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'asd', 1498385847, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ttin_hoctienganh`
--

CREATE TABLE `ttin_hoctienganh` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_khachhang` int(11) NOT NULL,
  `hoctrungtam` int(11) NOT NULL,
  `hoconline` int(11) DEFAULT NULL,
  `tentrungtam` varchar(255) DEFAULT NULL,
  `sobuoimottuan` int(11) DEFAULT NULL,
  `bomeduadon` int(11) DEFAULT NULL,
  `hocgviennuocngoai` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ttin_hoctienganh`
--

INSERT INTO `ttin_hoctienganh` (`id`, `id_khachhang`, `hoctrungtam`, `hoconline`, `tentrungtam`, `sobuoimottuan`, `bomeduadon`, `hocgviennuocngoai`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'eqwe', 2, 1, 1, 1498385847, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ttin_trangbi`
--

CREATE TABLE `ttin_trangbi` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_khachhang` int(11) NOT NULL,
  `laptop` int(11) NOT NULL,
  `pc` int(11) NOT NULL,
  `tainghe` int(11) NOT NULL,
  `micro` int(11) NOT NULL,
  `internet` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ttin_trangbi`
--

INSERT INTO `ttin_trangbi` (`id`, `id_khachhang`, `laptop`, `pc`, `tainghe`, `micro`, `internet`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, '1', 1498385847, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `group` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `last_login` int(11) NOT NULL,
  `login_hash` varchar(255) NOT NULL,
  `profile_fields` text NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `group`, `email`, `last_login`, `login_hash`, `profile_fields`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'NMa9DV/ro1Gjte2yQWZn/HygTy42pE7APE81Yn3gwGk=', 100, 'info@iSpeak.vn', 1498455139, 'bd40691be9641642d7d0289b244d14da97dbf832', 'a:1:{s:4:\"name\";s:11:\"Trung Đỗ\";}', 1498212882, 1498295064);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `khachhang`
--
ALTER TABLE `khachhang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `khachhang_statuses`
--
ALTER TABLE `khachhang_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `thongtinthem`
--
ALTER TABLE `thongtinthem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ttin_hoctienganh`
--
ALTER TABLE `ttin_hoctienganh`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ttin_trangbi`
--
ALTER TABLE `ttin_trangbi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `khachhang`
--
ALTER TABLE `khachhang`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `khachhang_statuses`
--
ALTER TABLE `khachhang_statuses`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `thongtinthem`
--
ALTER TABLE `thongtinthem`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ttin_hoctienganh`
--
ALTER TABLE `ttin_hoctienganh`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ttin_trangbi`
--
ALTER TABLE `ttin_trangbi`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
