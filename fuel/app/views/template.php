<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?= empty($title) ? 'Untitled' : $title ?></title>
	<base href="<?php echo URI::base() ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="noindex">
	<?php echo Asset::css(array(
		'bootstrap.min.css',
		'font-awesome.min.css',
		'pnotify.custom.min.css',
		'styles.css'
	)); 
	?>
	
	<?php echo Asset::js(array('jquery.min.js','bootstrap.min.js')) ?>
	<script>var base = $('base').attr('href')</script>
</head>
<body>
	<header>
		<nav class="navbar navbar-inverse" role="navigation" style="border-radius:0;">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo URI::base()?>" title="Trang chủ">DATA iSpeak</a>
				</div>
		
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dữ liệu khách hàng <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo Router::get('nhapdulieu') ?>">Nhập dữ liệu KH</a></li>
								<li><a href="<?php echo Router::get('xembaocao') ?>">Xem báo cáo</a></li>
							</ul>
						</li>

						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Liên hệ từ khách hàng <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo Router::get('customer_report_create') ?>">Thêm liên hệ từ KH</a></li>
								<li><a href="<?php echo Router::get('customer_report') ?>">Xem báo cáo</a></li>
							</ul>
						</li>

					</ul>

					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Hello, <?php echo $current_user->username ?: 'Anonymous'?>! <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<?php if (Auth::member(100)): ?>
									<li><a href="<?php echo Router::get('user_manage') ?>">Quản lí thành viên</a></li>
								<?php endif ?>
								<li><a href="<?php echo Router::get('canhan') ?>">Cá nhân</a></li>
								<li><a href="<?php echo Router::get('logout') ?>">Đăng xuất</a></li>
							</ul>
						</li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div>
		</nav>
	</header>	
	<div id="wrapper" class="container">
		<?php echo $content ?>
	</div>

	<?php
		$error_message = '';
		if (is_array(Session::get_flash('error'))){
			$errors = Session::get_flash('error');
			foreach ($errors as $key => $error) {
				if (is_object($error)){
					$error_message .= '- '. $error->get_message() .'<br>';
				}
			}
		}
		else $error_message = Session::get_flash('error');
	?>
	<script>
		var success = "<?= Session::get_flash('success') ?>";
		var notice = "<?= Session::get_flash('notice') ?>"
		var error = "<?= $error_message ?>";
		var isSticky = "<?= Session::get_flash('isSticky') ?>";
	</script>
	
	<?php echo Asset::js(array(
		'pnotify.custom.min.js',
		'main.js'
	))?>

	<?php
		switch (URI::current()) {
			case Router::get('user_manage'):
			case Router::get('customer_report'):
		 	case Router::get('xembaocao'):
		 		echo Asset::css('datatables.min.css');
		 		echo Asset::js(array('datatables.min.js','list.js'));
		 		break;
		 	default: 
		 		break;
		} 
	?>

</body>
</html>
