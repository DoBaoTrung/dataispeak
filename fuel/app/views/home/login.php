<!DOCTYPE html>
<html>

<head>
    <title>Trang đăng nhập mẫu</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans&amp;subset=vietnamese" rel="stylesheet">
    <?php echo Asset::css(array(
    	'font-awesome.min.css',
    	'pnotify.custom.min.css',
    	'login.css'
    )) ?>
</head>

<body>
    <div id="background">
	    <div id="form-container">
	    	<div class="inner">    		
		    	<div class="logo-container">
		    		<div class="line">
				    	<div class="outer o1">
				        	<span class="line-fill l1"></span>
				        </div>
				        <div class="outer o2">
				        	<span class="line-fill l2"></span>
				        </div>
				        <div class="outer o3">
				        	<span class="line-fill l3"></span>
				        </div>
			        </div>
			    	<div class="logo">
			            <a href="">
							<?php echo Asset::img('logo-white.png') ?>
			            </a>
			        </div>
		    	</div>
		    	
		        <form action="" method="POST" id="login-form">
					<div class="form-head">
						<h1 class="title">Login</h1>
					</div>
					<div class="body">
						

		        		<div class="form-group">
		        			<label for="email">Tên đăng nhập/email</label>
		        			<input type="text" name="email" class="email">
		        		</div>

		        		<div class="form-group">
		        			<label for="password">Mật khẩu</label>
		        			<input type="password" name="password" class="password">
		        		</div>
		        		
		        		<!-- <a href="" class="register">Bạn chưa có tài khoản? Đăng ký ngay</a>
						<a href="" class="forgot">Quên mật khẩu?</a> -->
		        		<button type="submit" class="login">Đăng nhập</button>
	    			</div>
			    </form>
		    </div>
	    </div>
    </div>
    <script type="text/javascript">
    	
    	window.onload = function(){
    		var line_fill = document.getElementsByClassName('line-fill');
    		for (var i = 0; i < line_fill.length; i++) {
    			line_fill[i].className += ' animate';
    		};

    		document.getElementsByClassName('logo')[0].className += ' animate';

    	}
	</script>

	<script>
		var success = "<?= Session::get_flash('success') ?>";
		var notice = "<?= Session::get_flash('notice') ?>"
		var error = "<?= Session::get_flash('error')?>";
		var isSticky = "<?= Session::get_flash('isSticky')?>";
	</script>
	
	<?php echo Asset::js(array(
		'jquery.min.js',
		'pnotify.custom.min.js',
		'main.js'
	)) ?>
</body>
</html>
