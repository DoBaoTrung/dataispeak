<h1 class="h1-home-title">HỆ THỐNG DỮ LIỆU QUẢN LÝ CHĂM SÓC KHÁCH HÀNG</h1>

<div class="home-info">
	<p><b>Nhân viên:</b> <?php echo Auth::get_profile_fields('name') ?: Auth::get('username')?></p>
	<p><b>Ngày:</b> <?php Helper::print_current_date_vi() ?></p>
</div>
<div class="clearfix"></div>
<div class="home-group">
	<h2 class="home-title">Dữ liệu khách hàng tiềm năng</h1>
	<a href="<?php echo Router::get('nhapdulieu')?>" class="btn btn-success">Nhập thông tin</a>
	<a href="<?php echo Router::get('xembaocao')?>" class="btn btn-success">Xem báo cáo</a>
</div>

<div class="home-group">
	<h2 class="home-title">Liên hệ từ khách hàng</h2>
	<a href="<?php echo Router::get('customer_report_create')?>" class="btn btn-warning">Nhập thông tin</a>
	<a href="<?php echo Router::get('customer_report')?>" class="btn btn-warning">Xem báo cáo</a>
</div>

<div class="home-group">
	<h2 class="home-title">Công việc được giao</h2>
	<a href="<?php echo Router::get('nhapdulieu')?>" class="btn btn-danger">Việc mới</a>
	<a href="<?php echo Router::get('xembaocao')?>" class="btn btn-danger">Xem tất</a>
</div>


