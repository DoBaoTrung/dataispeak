
<form action="" method="POST" role="form">
	<legend>Đổi thông tin cá nhân</legend>

	<div class="form-group">
		<label for="">Họ tên</label>
		<input type="text" name="name" value="<?php echo Auth::get_profile_fields('name') ? :'' ?>" class="form-control">
	</div>

	<div class="form-group">
		<label for="">Email</label>
		<input type="text" name="email" value="<?php echo Auth::get('email') ?>" class="form-control">
	</div>

	<div class="form-group">
		<label for="">Đổi mật khẩu</label>
		<input type="password" id="old-password" name="old_password" class="form-control" placeholder="Nhập mật khẩu hiện tại...">
	</div>

	<div id="new-password-group" style="display:none;">
		<div class="form-group">
			<label for="">Mật khẩu mới</label>
			<input type="password" name="new_password" class="form-control" placeholder="Mật khẩu mới">
		</div>

		<div class="form-group">
			<label for="">Nhập lại mật khẩu</label>
			<input type="password" name="confirm_password" class="form-control" placeholder="Xác nhận lại...">
		</div>
	</div>
	

	<button type="submit" class="btn btn-primary">Submit</button>
</form>

<script>
	$('#old-password').on('input', function(event) {
		if ($(this).val())
			$('#new-password-group').fadeIn();
		else $('#new-password-group').fadeOut();
	});
</script>