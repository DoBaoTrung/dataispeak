
	<h2 class="text-center">Danh sách <span class='muted'>trạng thái khách hàng</span></h2>
	<br>
	<?php if ($customer_statuses): ?>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Class</th>
				<th>Tác vụ</th>
			</tr>
		</thead>
		<tbody>
	<?php foreach ($customer_statuses as $item): ?>		<tr>

				<td><?php echo $item->name; ?></td>
				<td><?php echo $item->class; ?></td>
				<td>
					<div class="btn-toolbar">
						<div class="btn-group">
							<?php echo Html::anchor('customer/status/view/'.$item->id, '<i class="icon-eye-open"></i> View', array('class' => 'btn btn-default btn-sm')); ?>						<?php echo Html::anchor('customer/status/edit/'.$item->id, '<i class="icon-wrench"></i> Edit', array('class' => 'btn btn-default btn-sm')); ?>						<?php echo Html::anchor('customer/status/delete/'.$item->id, '<i class="icon-trash icon-white"></i> Delete', array('class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')")); ?>					</div>
					</div>

				</td>
			</tr>
	<?php endforeach; ?>	</tbody>
	</table>

	<?php else: ?>
	<p>No Customer_statuses.</p>

	<?php endif; ?><p>
		<?php echo Html::anchor('customer/status/create', 'Add new Customer status', array('class' => 'btn btn-success')); ?>

	</p>
