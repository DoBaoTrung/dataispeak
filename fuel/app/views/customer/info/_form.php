
<form action="" id="dataispeak-form" method="POST">
	<div class="row">
		<!-- Thông tin chung -->
		
		<div class="col-xs-12 col-sm-6 form-section">
			<h2 class="section-title">Thông tin chung</h2>
			<div class="form-group">
				<label for="">Họ và tên</label>
				<input value="<?= isset($customer) ? $customer->fullname : ''?>" type="text" name="fullname" class="form-control" required data-msg-required="Chưa nhập họ tên">
			</div>

			<div class="form-group">
				<label for="">Tuổi</label>
				<input value="<?= isset($customer) ? $customer->age : ''?>" type="number" name="age" class="form-control" required data-msg-required="Chưa nhập tuổi">
			</div>

			<div class="form-group">
				<label for="">Trạng thái</label>
				<select name="status" id="" class="form-control">
					<?php foreach (Model_Customer_Status::find('all') as $key => $item): ?>
						<?php if (isset($customer) && $key == $customer->status): ?>
								<option class="<?= $item->class ?>" value="<?= $key ?>" selected><?= $item->name?></option>
							<?php else: ?>
								<option class="<?= $item->class ?>" value="<?= $key ?>"><?= $item->name?></option>
						<?php endif ?>

						
					<?php endforeach ?>
				</select>
				
			</div>
		
			<div class="form-group">
				<label for="">Giới tính</label>
				<div class="radio">
					<label>
						Nam
						<input type="radio" name="sex" value="1" <?= (isset($customer) && $customer->sex == 1) ? 'checked="checked"' : '' ?> >				
					</label>
				</div>
				<div class="radio">
					<label>
						Nữ
						<input type="radio" name="sex" value="2" <?= (isset($customer) && $customer->sex == 2) ? 'checked="checked"' : '' ?> >				
					</label>
				</div>
			</div>

			<div class="form-group">
				<label for="">Địa chỉ</label>
				<input type="text" name="address" class="form-control address" value="<?= isset($customer) ? $customer->address : ''?>">

				<input type="number" value="<?= $bonus['address']?:'' ?>" name="bonus[address]" class="tuchamdiem form-control" placeholder="Tự cho điểm..." data-toggle="tooltip" min="0" max="2" title="Tối đa 2 điểm">
			</div>

			<div class="form-group">
				<label for="">Tên trường đang học</label>
				<input type="text" name="school_name" class="form-control school_name" value="<?= isset($customer) ? $customer->school_name : ''?>">
				<input type="number" value="<?= $bonus['school_name']?:'' ?>" name="bonus[school_name]" class="tuchamdiem form-control" placeholder="Tự cho điểm..." data-toggle="tooltip" min="0" max="3" title="Tối đa 3 điểm">
			</div>
			
			<div class="form-group">
				<label for="">Kết quả học tập chung ở trường</label>
				<div class="radio">
					<label>
						Xuất sắc
						<input type="radio" name="study_result" value="3" <?= (isset($customer) && $customer->study_result == 3) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
				<div class="radio">
					<label>
						Giỏi
						<input type="radio" name="study_result" value="2" <?= (isset($customer) && $customer->study_result == 2) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
				<div class="radio">
					<label>
						Khá
						<input type="radio" name="study_result" value="1" <?= (isset($customer) && $customer->study_result == 1) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
				<div class="radio">
					<label>
						Trung bình
						<input type="radio" name="study_result" value="0" <?= (isset($customer) && $customer->study_result == 0) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
			</div>
		</div>
		<!-- End ttc -->

		<!-- Học tiếng Anh -->
		<div class="col-xs-12 col-sm-6  form-section">
			<h2 class="section-title">Học tiếng Anh</h2>
			<div class="form-group">
				<label>Đã học tại trung tâm tiếng Anh nào chưa</label>
				<div class="radio">
					<label>
						Rồi
						<input type="radio" name="education_center" value="1" <?= (isset($customer) && $customer->ttin_hoctienganh->education_center == 1) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
				<div class="radio">
					<label>
						Chưa
						<input type="radio" name="education_center" value="0" <?= (isset($customer) && $customer->ttin_hoctienganh->education_center == 0) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
			</div>

			<div class="form-group">
				<label>Đã học tiếng Anh online bao giờ chưa</label>
				<div class="radio">
					<label>
						Rồi
						<input type="radio" name="online_learning" value="1" <?= (isset($customer) && $customer->ttin_hoctienganh->online_learning == 1) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
				<div class="radio">
					<label>
						Chưa
						<input type="radio" name="online_learning" value="0" <?= (isset($customer) && $customer->ttin_hoctienganh->online_learning == 0) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
			</div>
				
			<div class="form-group">
				<label for="">Tên trung tâm?</label>
				<input type="text" name="education_center_name" class="form-control" value="<?= isset($customer) ? $customer->ttin_hoctienganh->education_center_name : ''?>">
				<input type="number" value="<?= $bonus['education_center_name']?:'' ?>" name="bonus[education_center_name]" class="tuchamdiem form-control" placeholder="Tự cho điểm..." data-toggle="tooltip" min="0" max="3" title="Tối đa 3 điểm">
			</div>

			<div class="form-group">
				<label for="">Một tuần học thêm tiếng Anh bao nhiêu buổi</label>
				<input type="number" name="classes_per_week" class="form-control"  value="<?= isset($customer) ? $customer->ttin_hoctienganh->classes_per_week : ''?>">
			</div>


			<div class="form-group">
				<label>Bố mẹ có phải thường xuyên đưa đón không</label>
				<div class="radio">
					<label>
						Không
						<input type="radio" name="parents_pick_up" value="0" <?= (isset($customer) && $customer->ttin_hoctienganh->parents_pick_up == 0) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
				<div class="radio">
					<label>
						Thỉnh thoảng
						<input type="radio" name="parents_pick_up" value="1" <?= (isset($customer) && $customer->ttin_hoctienganh->parents_pick_up == 1) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
				<div class="radio">
					<label>
						Thường xuyên
						<input type="radio" name="parents_pick_up" value="2" <?= (isset($customer) && $customer->ttin_hoctienganh->parents_pick_up == 2) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
			</div>

			<div class="form-group">
				<label>Đã từng học tiếng Anh với giáo viên nước ngoài bao giờ chưa</label>
				<div class="radio">
					<label>
						Chưa bao giờ
						<input type="radio" name="learning_with_foreign_teachers" value="0" <?= (isset($customer) && $customer->ttin_hoctienganh->learning_with_foreign_teachers == 0) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
				<div class="radio">
					<label>
						Đã từng
						<input type="radio" name="learning_with_foreign_teachers" value="1" <?= (isset($customer) && $customer->ttin_hoctienganh->learning_with_foreign_teachers == 1) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
				<div class="radio">
					<label>
						Đang học
						<input type="radio" name="learning_with_foreign_teachers" value="2" <?= (isset($customer) && $customer->ttin_hoctienganh->learning_with_foreign_teachers == 2) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
			</div>
		</div>
		<!-- End hta -->
		
		<div class="clearfix"></div>

		<!-- Trang bị -->
		<div class="col-xs-12 col-sm-6  form-section">
			<h2 class="section-title">Trang bị</h2>
			<div class="form-group">
				<label>Có lap không?</label>
				<div class="radio">
					<label>
						Có
						<input type="radio" name="laptop" value="1" required <?= (isset($customer) && $customer->ttin_trangbi->laptop == 1) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
				<div class="radio">
					<label>
						Không
						<input type="radio" name="laptop" value="0" <?= (isset($customer) && $customer->ttin_trangbi->laptop == 0) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
			</div>

			<div class="form-group">
				<label>Có máy tính để bàn không?</label>
				<div class="radio">
					<label>
						Có
						<input type="radio" name="pc" value="1" required <?= (isset($customer) && $customer->ttin_trangbi->pc == 1) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
				<div class="radio">
					<label>
						Không
						<input type="radio" name="pc" value="0" <?= (isset($customer) && $customer->ttin_trangbi->pc == 0) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
			</div>

			<div class="form-group">
				<label>Có tai nghe không?</label>
				<div class="radio">
					<label>
						Có
						<input type="radio" name="headphone" value="1" required <?= (isset($customer) && $customer->ttin_trangbi->headphone == 1) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
				<div class="radio">
					<label>
						Không
						<input type="radio" name="headphone" value="0" <?= (isset($customer) && $customer->ttin_trangbi->laptop == 0) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
			</div>

			<div class="form-group">
				<label>Có Micro không?</label>
				<div class="radio">
					<label>
						Có
						<input type="radio" name="micro" value="1" required <?= (isset($customer) && $customer->ttin_trangbi->micro == 1) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
				<div class="radio">
					<label>
						Không
						<input type="radio" name="micro" value="0" <?= (isset($customer) && $customer->ttin_trangbi->laptop == 0) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
			</div>

			<div class="form-group">
				<label>Mạng gì</label>
				<div class="radio">
					<label>
						Tốt
						<input type="radio" name="internet" value="1" <?= (isset($customer) && $customer->ttin_trangbi->internet == 1) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
				<div class="radio">
					<label>
						Mạng miền núi
						<input type="radio" name="internet" value="0" <?= (isset($customer) && $customer->ttin_trangbi->internet == 0) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
			</div>
		</div>
		<!-- End trang bị -->

		<!-- Thông tin thêm -->
		<div class="col-xs-12 col-sm-6  form-section">
			<h2 class="section-title">Thông tin thêm</h2>
			<div class="form-group">
				<label>Bố/mẹ có thường xuyên học cùng con không</label>
				<div class="radio">
					<label>
						Thường xuyên
						<input type="radio" name="study_with_parents" value="2" <?= (isset($customer) && $customer->thongtinthem->study_with_parents == 2) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
				<div class="radio">
					<label>
						Thỉnh thoảng
						<input type="radio" name="study_with_parents" value="1" <?= (isset($customer) && $customer->thongtinthem->study_with_parents == 1) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
				<div class="radio">
					<label>
						Không
						<input type="radio" name="study_with_parents" value="0" <?= (isset($customer) && $customer->thongtinthem->study_with_parents == 0) ? 'checked="checked"' : '' ?> >
					</label>
				</div>
			</div>

			<div class="form-group">
				<label>Bố mẹ công tác trong lĩnh vực nào</label>
				<input type="text" name="parents_job" id="inputBmcttlvn" class="form-control" value="<?= isset($customer) ? $customer->thongtinthem->parents_job : ''?>">
				<input type="number" value="<?= $bonus['parents_job']?:'' ?>" name="bonus[parents_job]" class="tuchamdiem form-control" placeholder="Tự cho điểm..." data-toggle="tooltip" min="0" max="3" title="Tối đa 3 điểm">
			</div>
		</div>
		<!-- TTT -->

		<div class="col-xs-12 col-sm-6">
			<button class="btn btn-primary"><i class="fa fa-check-square-o"></i> Submit</button>
		</div>
	</div>
</form>

<?php echo Asset::js('jquery.validate.min.js') ?>
<?php echo Asset::js('ispeakdata.validate.js') ?>