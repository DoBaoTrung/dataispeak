<h1 class="title text-center">Danh sách khách hàng</h1>
<?php if (!empty($customer)): ?>

	<table id="table-kh" class="table table-bordered table-striped">
		<thead>
			<tr>
				<td>#</td>
				<td>Thao tác</td>
				<td>Họ và tên</td>
				<td>Trạng thái</td>
				<td title="Max là 40">Tiềm năng</td>
				<td>Giới tính</td>
				<td>Tuổi</td>
				<td>Địa chỉ</td>
				<td>Ngày tạo</td>
			</tr>
		</thead>
		<tbody>
			<?php $stt=1;foreach ($customer as $key => $kh): ?>
				<tr>
					<td><?= $stt++ ?></td>
					<td class="text-center">
						<form action="" method="POST">
							<a href="<?php echo Router::get('xemdulieu',array('id' => $key)) ?>" class="btn btn-sm btn-info" title="Xem">
								<i class="fa fa-eye"></i>
							</a>
							<a href="<?php echo Router::get('suadulieu',array('id' => $key)) ?>" class="btn btn-sm btn-warning" title="Sửa">
								<i class="fa fa-pencil"></i>
							</a>
							<button name="delete" value="<?= $key ?>" class="btn btn-sm btn-danger" title="Xoá" onclick="return confirm('Are you sure?')"><i class="fa fa-close"></i></button>
						</form>
					</td>
					<td><?php echo $kh->fullname ?></td>
					<td class="text-center">
						<span class="<?= $kh->customer_statuses->class ?> tag">
							<?php echo $kh->customer_statuses->name?>
						</span>
					</td>
					<td><?php echo $kh->potential_points ?></td>
					<td><?php Helper::print_gioitinh($kh->sex) ?></td>
					<td><?php echo $kh->age ?></td>
					<td><?php echo $kh->address ?></td>
					<td><?php echo date('d/m/Y',$kh->created_at) ?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
<?php else: ?>
	<p>Không có dữ liệu</p>
<?php endif ?>