<div id="view">
	<div class="row">
		<div class="col-xs-12 col-sm-7">
			<div class="row">
				<!-- Thông tin chung -->
				<div class="col-xs-12 form-section">
					<h2 class="section-title">Thông tin chung</h2>
					
					<div class="form-group">
						<label for="">Họ và tên</label>
						<p class="info"><?php echo $customer->fullname ?></p>
					</div>

					<div class="form-group">
						<label for="">Tuổi</label>
						<p class="info"><?php echo $customer->age ?></p>
					</div>
				
					<div class="form-group">
						<label for="">Giới tính</label>
						<p class="info"><?php Helper::print_gioitinh($customer->sex) ?></p>					
					</div>

					<div class="form-group">
						<label for="">Địa chỉ</label>
						<p class="info"><?php echo $customer->address ?></p>
					</div>

					<div class="form-group">
						<label for="">Tên trường đang học</label>
						<p class="info"><?php echo $customer->school_name ?></p>
					</div>
					
					<div class="form-group">
						<label for="">Kết quả học tập chung ở trường</label>
						<p class="info"><?php Helper::print_ketquahoctap($customer->study_result) ?></p>
					</div>
				</div>
				<!-- End ttc -->

				<!-- Học tiếng Anh -->
				<div class="col-xs-12 form-section">
					<h2 class="section-title">Học tiếng Anh</h2>
					<div class="form-group">
						<label>Đã học tại trung tâm tiếng Anh nào chưa</label>
						<p class="info">
							<?php Helper::print_roi_chua($customer->ttin_hoctienganh->education_center)?>
						</p>
					</div>

					<div class="form-group">
						<label>Đã học tiếng Anh online bao giờ chưa</label>
						<p class="info">
							<?php Helper::print_roi_chua($customer->ttin_hoctienganh->online_learning)?>
						</p>
					</div>

					<?php if ($customer->ttin_hoctienganh->education_center): ?>
						<div class="form-group">
							<label for="">Tên trung tâm?</label>
							<p class="info"><?php echo $customer->ttin_hoctienganh->education_center_name ?></p>
						</div>

						<div class="form-group">
							<label for="">Một tuần học thêm tiếng Anh bao nhiêu buổi</label>
							<p class="info"><?php echo $customer->ttin_hoctienganh->classes_per_week ?></p>
						</div>

						<div class="form-group">
							<label>Bố mẹ có phải thường xuyên đưa đón không</label>
							<p class="info"><?php Helper::print_co_khong($customer->ttin_hoctienganh->parents_pick_up) ?></p>
						</div>

						<div class="form-group">
							<label>Đã từng học tiếng Anh với giáo viên nước ngoài bao giờ chưa</label>
							<p class="info">
								<?php Helper::print_roi_chua($customer->ttin_hoctienganh->learning_with_foreign_teachers) ?>
							</p>
						</div>
					<?php endif ?>
				</div>
				<!-- End hta -->

				<!-- Trang bị -->
				<div class="col-xs-12 form-section">
					<h2 class="section-title">Trang bị</h2>
					<div class="form-group">
						<label>Có lap không?</label>
						<p class="info">
							<?php Helper::print_co_khong($customer->ttin_trangbi->laptop) ?>
						</p>
					</div>

					<div class="form-group">
						<label>Có máy tính để bàn không?</label>
						<p class="info">
							<?php Helper::print_co_khong($customer->ttin_trangbi->pc) ?>
						</p>
					</div>

					<div class="form-group">
						<label>Có tai nghe không?</label>
						<p class="info">
							<?php Helper::print_co_khong($customer->ttin_trangbi->headphone) ?>
						</p>
					</div>

					<div class="form-group">
						<label>Có Micro không?</label>
						<p class="info">
							<?php Helper::print_co_khong($customer->ttin_trangbi->micro) ?>
						</p>
					</div>

					<div class="form-group">
						<label>Mạng gì</label>
						<p class="info">
							<?php Helper::print_internet($customer->ttin_trangbi->internet) ?>
						</p>
					</div>
				</div>
				<!-- End trang bị -->

				<!-- Thông tin thêm -->
				<div class="col-xs-12 form-section">
					<h2 class="section-title">Thông tin thêm</h2>
					<div class="form-group">
						<label>Bố/mẹ có thường xuyên học cùng con không</label>
						<p class="info">
							<?php Helper::print_co_khong($customer->thongtinthem->study_with_parents) ?>
						</p>
					</div>

					<div class="form-group">
						<label>Bố mẹ công tác trong lĩnh vực nào</label>
						<p class="info">
							<?php Helper::print_co_khong($customer->thongtinthem->parents_job) ?>
						</p>
					</div>
					
					<div class="form-group">
						<label for="">Nhân viên tư vấn</label>
						<?php
							$profile_fields = unserialize(Model_User::find($customer->inputter)->profile_fields);
							$name = isset($profile_fields['name']) ? $profile_fields['name'] : 'Không rõ';
						?>
						<p class="info"><?php echo $name ?></p>
					</div>
				</div>
				<!-- TTT -->
				
				<div class="col-xs-12">
					<a class="btn btn-warning" href="<?php echo Router::get('suadulieu',array('id' => $customer->id)) ?>"><i class="fa fa-pencil"></i> Sửa thông tin</a>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-5">
			<div id="chart-container">		
				<h1 id="chart-title"><?php echo $customer->fullname ?></h1>
			    <canvas id="chart"></canvas>
			    <p class="rating">Khách hàng 
			    	<?php 
			    		$potential_points = $customer->potential_points;
			    		if ($potential_points>=35): 
			    	?>
			    		<strong>rất tiềm năng!</strong>

				    	<?php elseif ($potential_points >= 25 && $potential_points < 35): ?>

				    		<strong>tiềm năng</strong>

				    	<?php elseif ($potential_points >= 15 && $potential_points < 25): ?>

				    		<strong>thông thường</strong>

						<?php else: ?>
							<strong>ít tiềm năng</strong>
			    	<?php endif ?>
			    </p>
		    </div>
		</div>
	</div>
</form>

<script>
	var potential_points = <?= $customer->potential_points ?: 0 ?>;
</script>
<?php echo Asset::js(array('chart.min.js','ispeakdata.js')) ?>

