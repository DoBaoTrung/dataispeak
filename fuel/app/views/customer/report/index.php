<h2>Danh sách các <span class='text-center muted'>liên hệ từ khách hàng</span></h2>
<br>
<?php if ($customer_reports): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Ngày</th>
			<th>Giờ</th>
			<th>Tên học viên</th>
			<th>Hình thức thông báo</th>
			<th>Nhóm nội dung</th>
			<th>Nội dung</th>
			<th style="min-width: 210px;">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($customer_reports as $item): ?>		<tr>

			<td><?php echo date('d/m/Y',$item->date); ?></td>
			<td><?php echo date('H:i',$item->date); ?></td>
			<td><?php echo $item->customer_name; ?></td>
			<td><?php echo $item->report_type->name; ?></td>
			<td><?php echo $item->report_content_group->name; ?></td>
			<td><?php echo $item->content; ?></td>
			<td>
				<div class="btn-toolbar">
					<div class="btn-group">
						<?php echo Html::anchor('customer/report/view/'.$item->id, '<i class="fa fa-eye"></i> Xem', array('class' => 'btn btn-default btn-sm')); ?>						<?php echo Html::anchor('customer/report/edit/'.$item->id, '<i class="fa fa-pencil"></i> Sửa', array('class' => 'btn btn-default btn-sm')); ?>						<?php echo Html::anchor('customer/report/delete/'.$item->id, '<i class="fa fa-trash fa-inverse"></i> Xóa', array('class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')")); ?>					</div>
				</div>
			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>Chưa có liên hệ nào từ khách hàng.</p>

<?php endif; ?><p>
	<a href="<?php echo Router::get('customer_report_create')?>" class="btn btn-success"><i class="fa fa-plus"></i> Tạo mới</a>
</p>

