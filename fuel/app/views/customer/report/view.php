<h2>Xem liên hệ số <span class='muted'>#<?php echo $customer_report->id; ?></span></h2>

<p>
	<strong>Ngày:</strong>
	<?php echo date('d/m/Y',$customer_report->date); ?></p>
<p>
	<strong>Giờ:</strong>
	<?php echo date('H:i',$customer_report->date); ?></p>
<p>
	<strong>Tên khách hàng:</strong>
	<?php echo $customer_report->customer_name; ?></p>
<p>
	<strong>Hình thức thông báo:</strong>
	<?php echo $customer_report->report_type->name; ?></p>
<p>
	<strong>Nhóm vấn đề:</strong>
	<?php echo $customer_report->report_content_group->name; ?></p>
<p>
	<strong>Nội dung:</strong>
	<?php echo $customer_report->content; ?></p>

<?php echo Html::anchor('customer/report/edit/'.$customer_report->id, 'Sửa'); ?> |
<?php echo Html::anchor('customer/report', 'Trở về trang trước'); ?>