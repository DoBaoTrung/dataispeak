<h2>Listing <span class='muted'>Customer_report_contentgroups</span></h2>
<br>
<?php if ($customer_report_contentgroups): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($customer_report_contentgroups as $item): ?>		<tr>

			<td><?php echo $item->name; ?></td>
			<td>
				<div class="btn-toolbar">
					<div class="btn-group">
						<?php echo Html::anchor('customer/report/contentgroup/view/'.$item->id, '<i class="icon-eye-open"></i> View', array('class' => 'btn btn-default btn-sm')); ?>						<?php echo Html::anchor('customer/report/contentgroup/edit/'.$item->id, '<i class="icon-wrench"></i> Edit', array('class' => 'btn btn-default btn-sm')); ?>						<?php echo Html::anchor('customer/report/contentgroup/delete/'.$item->id, '<i class="icon-trash icon-white"></i> Delete', array('class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')")); ?>					</div>
				</div>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Customer_report_contentgroups.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('customer/report/contentgroup/create', 'Add new Customer report contentgroup', array('class' => 'btn btn-success')); ?>

</p>
