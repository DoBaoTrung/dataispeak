<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset class="col-xs-12">
		<div class="form-group">
			<?php echo Form::label('Thời gian', 'date', array('class'=>'control-label')); ?>

				<?php echo Form::input('date', Input::post('date', isset($customer_report) ? date('d/m/Y H:i',$customer_report->date) : date('d/m/Y H:i',time())), array('class' => 'col-md-4 datetime form-control', 'placeholder'=>'Date')); ?>

		</div>

		<div class="form-group">
			<?php echo Form::label('Tên học viên', 'customer_name', array('class'=>'control-label')); ?>

				<?php echo Form::input('customer_name', Input::post('customer_name', isset($customer_report) ? $customer_report->customer_name : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Customer name')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Hình thức thông báo', 'type', array('class'=>'control-label')); ?>
			
			<?php $first=1; foreach (Model_Customer_Report_Type::find('all') as $key => $item): ?>
				<?php if (isset($customer_report) && $customer_report->type == $item->id): ?>
					<!-- Loop radio button -->
					<div class="radio hidden">
						<label>
							<input type="radio" class="type" name="type" value="<?= $item->id?>" checked="checked" >
							<?php echo $item->name?>
						</label>
					</div>
					<!-- Loop masks -->
					<div class="btn mask btn-info" data-target="input[name='type']" data-target-type="radio" data-target-value="<?php echo $item->id?>"><?php echo $item->name ?></div>
				<?php else: ?>
					<!-- Loop radio button -->
					<div class="radio hidden">
						<label>
							<input type="radio" class="type" name="type" value="<?php echo $item->id ?>" <?= ($first==1 && !isset($customer_report)) ? 'checked="checked"' : ''?> >
							<?php echo $item->name;?>
						</label>
					</div>

					<!-- Loop masks -->
					<div class="btn mask <?= ($first==1 && !isset($customer_report)) ? 'btn-info' : 'btn-default'?>" data-target="input[name='type']" data-target-type="radio" data-target-value="<?php echo $item->id?>"><?php echo $item->name ?></div>

				<?php $first=0; endif ?>
			<?php endforeach ?>
			
		</div>

		<!-- content-group -->
		<div class="form-group">
			<?php echo Form::label('Nhóm vấn đề', 'content_group', array('class'=>'control-label')); ?>
			
			<?php $report_content_groups = Model_Customer_Report_Contentgroup::find('all'); ?>
			
			<!-- Loop select options -->
			<select name="content_group" class="form-control hidden" id="content-group">
				<?php foreach ($report_content_groups as $key => $item): ?>
					<?php if (isset($customer_report) && ($item->id == $customer_report->content_group)): ?>
							<option selected="selected" value="<?php echo $item->id?>"> <?php echo $item->name ?></option>
						<?php else: ?>
							<option value="<?php echo $item->id?>"> <?php echo $item->name ?></option>
					<?php endif ?>
				<?php endforeach ?>
			</select>

			<!-- Loop masks -->
			<?php $first=1;foreach ($report_content_groups as $key => $item): ?>
				<?php if (isset($customer_report) && ($item->id == $customer_report->content_group)): ?>
						<div class="btn mask btn-info" data-target="#content-group" data-target-type="select" data-target-value="<?= $item->id?>">
							<?php echo $item->name ?>
						</div>
					<?php else: ?>
						<div class="btn mask <?= ($first==1 && !isset($customer_report)) ? 'btn-info' : 'btn-default' ?>" data-target="#content-group" data-target-type="select" data-target-value="<?= $item->id?>">
							<?php echo $item->name; $first=0; ?>
						</div>
				<?php endif ?>
			<?php endforeach ?>
		</div>
		<!-- end content-group -->

		<div class="form-group">
			<?php echo Form::label('Nội dung', 'content', array('class'=>'control-label')); ?>

				<?php echo Form::textarea('content', Input::post('content', isset($customer_report) ? $customer_report->content : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'Content')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo Form::close(); ?>

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/excite-bike/jquery-ui.min.css">
<?php echo Asset::css('jquery-ui-timepicker-addon.css') ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<?php echo Asset::js(array('jquery-ui-timepicker-addon.js','customer/report.js')) ?>
