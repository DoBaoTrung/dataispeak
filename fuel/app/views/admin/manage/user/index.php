<h1>Quản lý đội ngũ cskh</h1>


<div class="modal fade" id="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<table style="margin-top:25px;" class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>Tên đăng nhập</th>
			<th>Email</th>
			<th>Họ tên</th>
			<th>Vị trí</th>
			<th>Tạo ngày</th>
			<th>Thao tác</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($users as $key => $user): ?>
			<tr>
				<td> <?php echo $key ?></td>
				<td> <?php echo $user->username ?></td>
				<td> <?php echo $user->email ?></td>
				<td>
					<?php
					 	echo isset($user->profile_fields['name']) ? $user->profile_fields['name'] : 'Anonymous';
					?>
				</td>
				<td>
					<?php echo isset($user->profile_fields['job_position']) ? $user->profile_fields['job_position'] : 'Chưa rõ' ?>
				</td>
				<td> <?php echo date('d/m/Y', $user->created_at) ?></td>
				<td style="width:250px;">
					<div class="btn-toolbar">
						<div class="btn-group">
							<a class="get_job_description btn btn-sm btn-default" data-user-id="<?php echo $user->id ?>" data-toggle="modal" href='#modal'><i class="fa fa-sticky-note"></i> Mô tả công việc</a>
							<a href="<?php echo Router::get('user_manage_edit',array('id'=> $user->id))?>" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i> Sửa</a>
							<a href="<?php echo Router::get('user_manage_delete',array('id'=> $user->id))?>" class="btn btn-sm btn-danger" onclick="return confirm('Sure?')">
								<i class="fa fa-times"></i> Xoá
							</a>
						</div>
					</div>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>

<p><a href="<?php echo Router::get('user_manage_create') ?>" class="btn btn-md btn-success"><i class="fa fa-plus"></i> Thêm nhân viên mới</a></p>

<script>
	$('.get_job_description').click(function(event) {
		event.preventDefault();
		$.ajax({
			url: '<?= Router::get("user_manage_get_job_description")?>',
			type: 'GET',
			dataType: 'json',
			data: {'user_id': $(this).data('user-id')},
			success: function(result){
				$('#modal .modal-header .modal-title').html('Mô tả công việc của ' + result['profile_fields']['name']);
				$('#modal .modal-body').html(result['profile_fields']['job_description']);
			}
		})	
	});
</script>