<?php echo Form::open(array("class"=>"form-horizontal","autocomplete"=> "off")); ?>

	<fieldset class="col-xs-12 col-sm-6">

		<?php if (!isset($user)): ?>
			<div class="form-group">
				<label for="username">Tên đăng nhập</label>
				<input type="text" value="<?= isset($user) ? $user->username : ''?>" name="username" id="inputUsername" class="form-control" autofocus required>
			</div>

			<div class="form-group">
				<label for="password">Mật khẩu</label>
				<input type="password" autocomplete="new-password" name="password" id="inputPassword" class="form-control" required>
			</div>
		<?php endif ?>
		

		<div class="form-group">
			<label for="email">Email</label>
			<input type="text" value="<?= isset($user) ? $user->email : ''?>" name="email" id="inputEmail" class="form-control" required>
		</div>

		<div class="form-group">
			<label for="profile_fields[name]">Họ và tên</label>
			<?php if (isset($user->profile_fields['name'])): ?>
				<input type="text" name="profile_fields[name]" value="<?php echo $user->profile_fields['name'] ?>" class="form-control">
			<?php else: ?>
				<input type="text" name="profile_fields[name]" class="form-control">
			<?php endif ?>
			
		</div>

		<div class="form-group">
			<label for="profile_fields[job_position]">Vị trí công việc</label>
			<?php if (isset($user->profile_fields['job_position'])): ?>
				<input type="text" value="<?php echo $user->profile_fields['job_position'] ?>" name="profile_fields[job_position]" class="form-control">
			<?php else: ?>
				<input type="text" name="profile_fields[job_position]" class="form-control">
			<?php endif ?>
		</div>

		<div class="form-group">
			<label for="profile_fields[job_description]">Mô tả công việc</label>
			<?php if (isset($user->profile_fields['job_description'])): ?>
				<textarea name="profile_fields[job_description]" rows="5" class="form-control"><?php echo $user->profile_fields['job_description']?></textarea>
			<?php else: ?>
				<textarea name="profile_fields[job_description]" rows="5" class="form-control"></textarea>
			<?php endif ?>
		</div>

		<div class="form-group">
			<button class="btn btn-sm btn-primary">Lưu</button> &nbsp;
			<?php if (isset($user)): ?>
				<button name="reset_password" value=" " class="btn btn-sm btn-warning" onclick="return confirm('Sure?')">
					Reset mật khẩu
				</button>
			<?php endif ?>

			<p style="margin-top:15px"><a href="<?php echo Router::get('user_manage') ?>">Trở về trang trước</a></p>
		</div>
	</fieldset>
	
<?php echo Form::close(); ?>

