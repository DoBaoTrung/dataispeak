<?php
return array(
	'_root_'  => 'home/index',  // The default route
	'_404_'   => 'home/404',    // The main 404 route
	
// secret
	'secret-update' => array('secret/update/index', 'name' => 'updater'),

// admin
	// manage users
	'quan-ly-thanh-vien' => array('admin/manage/user/index','name' => 'user_manage'),
	'quan-ly-thanh-vien/them' => array('admin/manage/user/create','name' => 'user_manage_create'),
	'quan-ly-thanh-vien/sua/(:id)' => array('admin/manage/user/edit/$1','name' => 'user_manage_edit'),
	'quan-ly-thanh-vien/xoa/(:id)' => array('admin/manage/user/delete/$1','name' => 'user_manage_delete'),
	'get-job-description' => array('admin/manage/user/get_job_description' ,'name'=> 'user_manage_get_job_description'),

// user
	'ca-nhan' => array('user/index', 'name' => 'canhan'),
	'login' => array('authorize/login', 'name' => 'login'),
	'logout' => array('authorize/logout', 'name' => 'logout'),

// khachhang
	// INFO
	'nhap-du-lieu' => array('customer/info/create', 'name' => 'nhapdulieu'),
	'khach-hang/(:id)' => array('customer/info/view/$1' , 'name' => 'xemdulieu'),
	'sua-thong-tin-khach-hang/(:id)' => array('customer/info/edit/$1', 'name' => 'suadulieu'),
	'xoa-khach-hang' => array('customer/info/delete', 'name' => 'xoadulieu'),
	'xem-bao-cao' => array('customer/info/list', 'name'=> 'xembaocao'),

	// STATUSES
	'danh-sach-trang-thai-khach-hang' => array('customer/status', 'name' => 'trangthaicustomer'),
	
	// REPORTS
	'lien-he-tu-khach-hang' => array('customer/report/index','name' => 'customer_report'),
	'lien-he-tu-khach-hang/tao-moi' => array('customer/report/create','name' => 'customer_report_create'),
	'lien-he-tu-khach-hang/xem/(:id)' => array('customer/report/view/$1', 'name' => 'customer_report_view'),
	'lien-he-tu-khach-hang/sua/(:id)' => array('customer/report/edit/$1', 'name' => 'customer_report_edit')

);
