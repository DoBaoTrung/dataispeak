<?php
return array (
  'version' => 
  array (
    'app' => 
    array (
      'default' => 
      array (
        0 => '001_create_users',
        1 => '002_create_customers',
        2 => '003_create_customer_info_learningenglish',
        3 => '004_create_customer_info_equipments',
        4 => '005_create_customer_info_other',
        5 => '006_create_customer_statuses',
        6 => '007_create_customer_reports',
        7 => '008_create_customer_report_contentgroups',
        8 => '009_create_customer_report_types',
      ),
    ),
    'module' => 
    array (
    ),
    'package' => 
    array (
    ),
  ),
  'folder' => 'migrations/',
  'table' => 'migration',
);
