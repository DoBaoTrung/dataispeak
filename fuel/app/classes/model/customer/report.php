<?php
use Orm\Model;

class Model_Customer_Report extends Model
{
	protected static $_properties = array(
		'id',
		'date',
		'customer_name',
		'type',
		'content_group',
		'content',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_belongs_to = array(
		'report_content_group' => array(
	        'key_from' => 'content_group',
	        'model_to' => 'Model_Customer_Report_Contentgroup',
	        'key_to' => 'id',
	        'cascade_save' => true,
	        'cascade_delete' => false,
	    ),
	    'report_type' => array(
	        'key_from' => 'type',
	        'model_to' => 'Model_Customer_Report_Type',
	        'key_to' => 'id',
	        'cascade_save' => true,
	        'cascade_delete' => false,
	    ),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('date', 'Thời gian', 'required');
		$val->add_field('customer_name', 'Tên khách hàng', 'required|max_length[255]');
		$val->add_field('type', 'Hình thức thông báo', 'required|valid_string[numeric]');
		$val->add_field('content_group', 'Nhóm vấn đề', 'required|valid_string[numeric]');
		$val->add_field('content', 'Nội dung', 'required');
		$val->set_message('required', 'Chưa nhập :label.');
		return $val;
	}

}
