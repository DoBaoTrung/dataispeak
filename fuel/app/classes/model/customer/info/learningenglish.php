<?php

class Model_Customer_Info_Learningenglish extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'customer_id',
		'education_center',
		'online_learning',
		'education_center_name',
		'classes_per_week',
		'parents_pick_up',
		'learning_with_foreign_teachers',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_belongs_to = array(
	    'customer' => array(
	        'key_from' => 'customer_id',
	        'model_to' => 'Model_Customer',
	        'key_to' => 'id',
	        'cascade_save' => true,
	        'cascade_delete' => false,
	    )
	);

	protected static $_table_name = 'customer_info_learningenglish';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('education_center', 'Đã học tiếng Anh ở trung tâm nào chưa', 'required');
		return $val;
	}
}
