<?php

class Model_Customer_Info_Equipment extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'customer_id',
		'laptop',
		'pc',
		'headphone',
		'micro',
		'internet',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_belongs_to = array(
	    'customer' => array(
	        'key_from' => 'customer_id',
	        'model_to' => 'Model_Customer',
	        'key_to' => 'id',
	        'cascade_save' => true,
	        'cascade_delete' => false,
	    )
	);

	protected static $_table_name = 'customer_info_equipments';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('laptop', 'Có laptop không', 'required');
		$val->add_field('pc', 'Có máy tính để bàn không', 'required');
		$val->add_field('headphone', 'Có tai nghe chưa', 'required');
		$val->add_field('micro', 'Có micro chưa', 'required');
		return $val;
	}
}
