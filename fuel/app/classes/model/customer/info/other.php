<?php

class Model_Customer_Info_Other extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'customer_id',
		'study_with_parents',
		'parents_job',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_belongs_to = array(
	    'customer' => array(
	        'key_from' => 'customer_id',
	        'model_to' => 'Model_Customer',
	        'key_to' => 'id',
	        'cascade_save' => true,
	        'cascade_delete' => false,
	    )
	);

	protected static $_table_name = 'customer_info_other';

}
