<?php
use Orm\Model;

class Model_Customer_Status extends Model
{
	protected static $_properties = array(
		'id',
		'name',
		'class',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_has_many = array(
	    'customer' => array(
	        'key_from' => 'id',
	        'model_to' => 'Model_Customer',
	        'key_to' => 'status',
	        'cascade_save' => true,
	        'cascade_delete' => false,
	    )
	);

	protected static $_table_name = 'customer_statuses';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('name', 'Name', 'required|max_length[255]');
		return $val;
	}

}
