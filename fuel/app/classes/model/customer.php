<?php

class Model_Customer extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'fullname',
		'name',
		'sex',
		'age',
		'address',
		'school_name',
		'study_result',
		'potential_points',
		'inputter',
		'created_at',
		'updated_at',
		'status',
		'bonus_points'
	);

	protected static $_has_one = array(
	    'ttin_hoctienganh' => array(
	        'key_from' => 'id',
	        'model_to' => 'Model_Customer_Info_Learningenglish',
	        'key_to' => 'customer_id',
	        'cascade_save' => true,
	        'cascade_delete' => true,
	    ),
	    'thongtinthem' => array(
	        'key_from' => 'id',
	        'model_to' => 'Model_Customer_Info_Other',
	        'key_to' => 'customer_id',
	        'cascade_save' => true,
	        'cascade_delete' => true,
	    ),
	    'ttin_trangbi' => array(
	        'key_from' => 'id',
	        'model_to' => 'Model_Customer_Info_Equipment',
	        'key_to' => 'customer_id',
	        'cascade_save' => true,
	        'cascade_delete' => true,
	    ),
	);

	protected static $_belongs_to = array(
		'customer_statuses' => array(
	    	'key_from' => 'status',
	        'model_to' => 'Model_Customer_Status',
	        'key_to' => 'id',
	        'cascade_save' => false,
	        'cascade_delete' => false,
	    )
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'customers';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('fullname', 'Họ và tên', 'required|valid_string[\'utf8\']');
		$val->add_field('age', 'Tuổi', 'required');
		return $val;
	}

}
