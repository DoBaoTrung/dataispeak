<?php

class Model_User extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'username',
		'password',
		'group',
		'email',
		'last_login',
		'login_hash',
		'profile_fields',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'users';

	public static function validate_password($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('new_password', 'Mật khẩu mới', 'required|min_length[8]');
		return $val;
	}


	/**
	 * Lấy toàn bộ thông tin người dùng
	 * @param  array  $select_array [Mảng những trường muốn select]
	 * @return [mix]               [Toàn bộ thông tin người dùng]
	 */
	public static function get_all_users($select_array = array()){
		if (empty($select_array)){
			$select_array = array('username','email','profile_fields','created_at');
		}
		$users = Model_User::query()->where('id', '!=', Auth::get('id'))
									->select($select_array)
									->get();
		foreach ($users as $key => $user) {
			if (!empty($user->profile_fields)){
				$user->profile_fields = @unserialize($user->profile_fields);
			}
		}
		return $users;
	}


	/**
	 * Lấy thông tin một người dùng
	 * @param  [int] $id           [id người dùng]
	 * @param  array  $select_array [Mảng những trường muốn select]
	 * @return [mixed]               [Thông tin người dùng]
	 */
	public static function get_one_user($id, $select_array = array()){
		if (empty($select_array)){
			$select_array = array('username','email','profile_fields','created_at');
		}
		$user =  Model_User::query()->where('id','=',$id)
									->select($select_array)
									->get_one();
		if (!empty($user->profile_fields)){
			$user->profile_fields = @unserialize($user->profile_fields);
		}
		return $user;
	}
}
