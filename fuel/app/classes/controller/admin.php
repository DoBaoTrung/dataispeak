<?php 

class Controller_Admin extends Controller_Home
{
	public $template = 'template';

	public function before(){
		parent::before();
		if (!Auth::member(100)){
			Session::set_flash('notice','Bạn không có quyền truy cập trang quản lý');
			Response::redirect();
		}
	}
}
