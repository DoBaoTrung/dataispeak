<?php
class Controller_Customer_Report extends Controller_Home
{

	public function action_index()
	{
		$data['customer_reports'] = Model_Customer_Report::find('all');
		$this->template->title = "Danh sách liên hệ từ phía khách hàng";
		$this->template->content = View::forge('customer/report/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('customer/report');

		if ( ! $data['customer_report'] = Model_Customer_Report::find($id))
		{
			Session::set_flash('error', 'Could not find customer_report #'.$id);
			Response::redirect('customer/report');
		}

		$this->template->title = "Customer_report";
		$this->template->content = View::forge('customer/report/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Customer_Report::validate('create');
			if ($val->run())
			{
				$dt = DateTime::createFromFormat('d/m/Y H:i', Input::post('date'));
				$customer_report = Model_Customer_Report::forge(array(
					'date' => $dt ? $dt->getTimestamp() : 0,
					'customer_name' => Input::post('customer_name'),
					'type' => Input::post('type'),
					'content_group' => Input::post('content_group'),
					'content' => Input::post('content'),
				));

				if ($customer_report and $customer_report->save())
				{
					Session::set_flash('success', 'Added customer_report #'.$customer_report->id.'.');

					Response::redirect('customer/report');
				}

				else
				{
					Session::set_flash('error', 'Could not save customer_report.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Thêm mới liên hệ từ khách hàng";
		$this->template->content = View::forge('customer/report/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('customer/report');

		if ( ! $customer_report = Model_Customer_Report::find($id))
		{
			Session::set_flash('error', 'Could not find customer_report #'.$id);
			Response::redirect('customer/report');
		}

		$val = Model_Customer_Report::validate('edit');

		if ($val->run())
		{
			$dt = DateTime::createFromFormat('d/m/Y H:i', Input::post('date'));
			$customer_report->date = $dt ? $dt->getTimestamp() : 0;
			$customer_report->customer_name = Input::post('customer_name');
			$customer_report->type = Input::post('type');
			$customer_report->content_group = Input::post('content_group');
			$customer_report->content = Input::post('content');

			if ($customer_report->save())
			{
				Session::set_flash('success', 'Updated customer_report #' . $id);

				Response::redirect('customer/report');
			}

			else
			{
				Session::set_flash('error', 'Could not update customer_report #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$customer_report->date = $val->validated('date');
				$customer_report->customer_name = $val->validated('customer_name');
				$customer_report->type = $val->validated('type');
				$customer_report->content_group = $val->validated('content_group');
				$customer_report->content = $val->validated('content');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('customer_report', $customer_report, false);
		}

		$this->template->title = "Sửa liên hệ từ khách hàng";
		$this->template->content = View::forge('customer/report/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('customer/report');

		if ($customer_report = Model_Customer_Report::find($id))
		{
			$customer_report->delete();

			Session::set_flash('success', 'Đã xoá #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete customer_report #'.$id);
		}

		Response::redirect('customer/report');

	}

}
