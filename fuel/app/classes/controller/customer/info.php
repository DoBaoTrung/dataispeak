<?php 

	class Controller_Customer_Info extends Controller_Home
	{

		private $view_per_page = 50;

		public function before(){
			parent::before();
		}

		public function action_create(){
			if (Input::method()== 'POST'){
				$val1 = Model_Customer::validate('thongtincoban');
				$val2 = Model_Customer_Info_Learningenglish::validate('ttin_hoctienganh');
				$val3 = Model_Customer_Info_Equipment::validate('ttin_trangbi');
				if ($val1->run() && $val2->run() && $val3->run()){
					$potential_points = $this->tinh_diem();
					
					$thongtincoban = Model_Customer::forge(array(
						'fullname' => Input::post('fullname'),
						'name' => trim(strrchr(Input::post('fullname'),' ')),
						'sex' => Input::post('sex'),
						'age' => Input::post('age'),
						'address' => Input::post('address'),
						'school_name' => Input::post('school_name'),
						'study_result' => Input::post('study_result'),
						'potential_points' => $potential_points,
						'inputter' => Auth::get('id'),
						'status' => Input::post('status'),
						'bonus_points' => json_encode(Input::post('bonus'))
					));
					
					$result1 = $thongtincoban->save();

					$ttin_hoctienganh = Model_Customer_Info_Learningenglish::forge(array(
						'customer_id' => $thongtincoban->id,
						'education_center' => Input::post('education_center'),
						'online_learning' => Input::post('online_learning'),
						'education_center_name' => Input::post('education_center_name'),
						'classes_per_week' => Input::post('classes_per_week'),
						'parents_pick_up' => Input::post('parents_pick_up'),
						'learning_with_foreign_teachers' => Input::post('learning_with_foreign_teachers')
					));
					$result2 = $ttin_hoctienganh->save();

					$ttin_trangbi = Model_Customer_Info_Equipment::forge(array(
						'customer_id' => $thongtincoban->id,
						'laptop' => Input::post('laptop'),
						'pc' => Input::post('pc'),
						'headphone' => Input::post('headphone'),
						'micro' => Input::post('micro'),
						'internet' => Input::post('internet')
					));
					$result3 = $ttin_trangbi->save();

					$thongtinthem = Model_Customer_Info_Other::forge(array(
						'customer_id' => $thongtincoban->id,
						'study_with_parents' => Input::post('study_with_parents'),
						'parents_job' => Input::post('parents_job')
					));
					$result4 = $thongtinthem->save();

					if ($result1 && $result2 && $result3 && $result4){
						Session::set_flash('success', 'Lưu thành công!');
					}
					else Session::set_flash('notice', 'Đã có lỗi xảy ra.');
				}
				else {
					$error_message = '';
					if ($val1->error()){
						foreach ($val1->error() as $field => $error)
					    {
					        $error_message.= '- '. $error->get_message(':label không hợp lệ!').'<br>';
					    }
					}
					if ($val2->error()){
						foreach ($val2->error() as $field => $error)
					    {
					        $error_message.= '- '. $error->get_message(':label không hợp lệ!').'<br>';
					    }
					}
					if ($val3->error()){
						foreach ($val3->error() as $field => $error)
					    {
					        $error_message.= '- '. $error->get_message(':label không hợp lệ!').'<br>';
					    }
					}
					Session::set_flash('error',$error_message);
				}			
			}
			$this->template->set_global('bonus', null);
			$this->template->title = 'Nhập dữ liệu khách hàng';
			$this->template->content = View::forge('customer/info/create');		
		}

		public function action_edit($id){
			$customer = Model_Customer::find($id);
			if (!$customer){
				Session::set_flash('error','Khách hàng không tồn tại.');
				Response::redirect(Router::get('xembaocao'));
			}

			if (Input::method()== 'POST'){
				$val1 = Model_Customer::validate('thongtincoban');
				$val2 = Model_Customer_Info_Learningenglish::validate('ttin_hoctienganh');
				$val3 = Model_Customer_Info_Equipment::validate('ttin_trangbi');
				if ($val1->run() && $val2->run() && $val3->run()){
					$potential_points = $this->tinh_diem();

					$customer->fullname = Input::post('fullname');
					$customer->name = trim(strrchr(Input::post('fullname'),' '));
					$customer->sex = Input::post('sex');
					$customer->age = Input::post('age');
					$customer->address = Input::post('address');
					$customer->school_name = Input::post('school_name');
					$customer->study_result = Input::post('study_result');
					$customer->potential_points = $potential_points;
					$customer->status = Input::post('status');
					$customer->bonus_points = json_encode(Input::post('bonus'));
					
					$customer->ttin_hoctienganh->education_center = Input::post('education_center');
					$customer->ttin_hoctienganh->online_learning = Input::post('online_learning');
					$customer->ttin_hoctienganh->education_center_name = Input::post('education_center_name');
					$customer->ttin_hoctienganh->classes_per_week = Input::post('classes_per_week');
					$customer->ttin_hoctienganh->parents_pick_up = Input::post('parents_pick_up');
					$customer->ttin_hoctienganh->learning_with_foreign_teachers = Input::post('learning_with_foreign_teachers');

					$customer->ttin_trangbi->laptop = Input::post('laptop');
					$customer->ttin_trangbi->pc = Input::post('pc');
					$customer->ttin_trangbi->headphone = Input::post('headphone');
					$customer->ttin_trangbi->micro = Input::post('micro');
					$customer->ttin_trangbi->internet = Input::post('internet');
		
					$customer->thongtinthem->study_with_parents = Input::post('study_with_parents');
					$customer->thongtinthem->parents_job = Input::post('parents_job');

					if ($customer->save()){
						Session::set_flash('success', 'Lưu thành công!');
					}
					else Session::set_flash('notice', 'Đã có lỗi xảy ra.');
				}
				else {
					$error_message = '';
					if ($val1->error()){
						foreach ($val1->error() as $field => $error)
					    {
					        $error_message.= '- '. $error->get_message(':label không hợp lệ!').'<br>';
					    }
					}
					if ($val2->error()){
						foreach ($val2->error() as $field => $error)
					    {
					        $error_message.= '- '. $error->get_message(':label không hợp lệ!').'<br>';
					    }
					}
					if ($val3->error()){
						foreach ($val3->error() as $field => $error)
					    {
					        $error_message.= '- '. $error->get_message(':label không hợp lệ!').'<br>';
					    }
					}
					Session::set_flash('error',$error_message);
				}			
			}

			$this->template->set_global('bonus', json_decode($customer->bonus_points,true));
			$this->template->set_global('customer',$customer);
			$this->template->title = 'Sửa thông tin khách hàng';
			$this->template->content = View::forge('customer/info/edit');
		}

		public function action_view($id){
			$customer = Model_Customer::find($id);
			if (empty($customer)){
				return Response::redirect(Router::get('xembaocao'));
			}
			$this->template->set_global('customer',$customer);
			$this->template->title = 'Chi tiết khách hàng';
			$this->template->content = View::forge('customer/info/view');
		}

		public function action_list(){
			if (Input::method()== 'POST'){
				if (!empty(Input::post('delete'))){
					$id = Input::post('delete');
					$this->delete_customer($id);
				}
			}

			// $current_page = Input::get('page') ?: 1;
			// $view_per_page = $this->view_per_page;

			// $data['customer'] = Model_Customer::query()->offset($view_per_page * ($current_page-1))->limit($view_per_page)->get('all');

			$data['customer'] = Model_Customer::query()->get('all');

			// PAGINATION
			// $data['totalKH'] = Model_Customer::count();
			// $data['totalPages'] = ceil($data['totalKH'] / $this->view_per_page);

			$this->template->title = 'Danh sách khách hàng';
			$this->template->content = View::forge('customer/info/list', $data);
		}

		private function delete_customer($id){
			$khach = Model_Customer::find($id);
			if ($khach->delete()){
				Session::set_flash('notice','DELETED');
			};
			Response::redirect(Router::get('xembaocao'));
		}

		/**
		 * Tính điểm!
		 * @return [int] [Điểm sau khi tính]
		 */
		private function tinh_diem(){
			$potential_points = 0;
			// fullname
			if (Input::post('fullname')) {
				$potential_points += 2;
			}

			// gioi tinh
			if (Input::post('sex')){
				$potential_points +=2;
			}

			// age
			if (Input::post('age')){
				$age = Input::post('age');
				if ($age >= 6 && $age < 10) {
					$potential_points+= 4;
				}
				else if ($age >= 10 && $age < 13) $potential_points+= 3;
				else if ($age >= 13) $potential_points+=2;
			}

			// ket qua hoc tap
			if (Input::post('study_result')){
				switch (Input::post('study_result')) {
					// Khá
					case 1:
						$potential_points += 1;
						break;
					// Giỏi
					case 2:
						$potential_points += 2.5;
						break;
					// Xuất sắc
					case 3:
						$potential_points += 3;
						break;
					default:
						# code...
						break;
				}
			}


			if (Input::post('education_center')){
				$potential_points += 2;
			}

			if (Input::post('online_learning')){
				$potential_points += 2;
			}

			if (Input::post('classes_per_week')){
				$classes_per_week = Input::post('classes_per_week');

				if ($classes_per_week==1 || $classes_per_week == 2){
					$potential_points += 2;
				}
				else if ($classes_per_week > 2){
					$potential_points += 1;
				}
			}

			if (Input::post('parents_pick_up')){
				switch (Input::post('parents_pick_up')) {
					// Thường xuyên
					case 2:
						$potential_points += 3;
						break;
					// Thỉnh thoảng
					case 1:
						$potential_points += 1;
						break;
					default:
						break;
				}
			}

			if (Input::post('learning_with_foreign_teachers')){
				switch (Input::post('learning_with_foreign_teachers')) {
					// Đang học
					case 2:
						$potential_points += 2;
						break;
					// Đã từng học
					case 1:
						$potential_points += 1;
						break;
					default:
						break;
				}
			}

			if (Input::post('laptop')){
				$potential_points += 1;
			}

			if (Input::post('pc')){
				$potential_points += 1;
			}

			if (Input::post('headphone')){
				$potential_points += 1;
			}

			if (Input::post('micro')){
				$potential_points += 1;
			}

			if (Input::post('internet')){
				$potential_points += 1;
			}

			if (Input::post('study_with_parents')){
				switch (Input::post('study_with_parents')) {
					case '1':
						$potential_points+=1;
						break;
					case '2':
						$potential_points+=2;
						break;
					default:
						# code...
						break;
				}
			}

			// Điểm tự đánh giá
			$bonus = 0;
			foreach (Input::post('bonus') as $key => $value) {
				$bonus += $value;
			}
			return $potential_points + $bonus;
		}
	}
 ?>