<?php
class Controller_Customer_Report_Contentgroup extends Controller_Home
{

	public function action_index()
	{
		$data['customer_report_contentgroups'] = Model_Customer_Report_Contentgroup::find('all');
		$this->template->title = "Customer_report_contentgroups";
		$this->template->content = View::forge('customer/report/contentgroup/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('customer/report/contentgroup');

		if ( ! $data['customer_report_contentgroup'] = Model_Customer_Report_Contentgroup::find($id))
		{
			Session::set_flash('error', 'Could not find customer_report_contentgroup #'.$id);
			Response::redirect('customer/report/contentgroup');
		}

		$this->template->title = "Customer_report_contentgroup";
		$this->template->content = View::forge('customer/report/contentgroup/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Customer_Report_Contentgroup::validate('create');

			if ($val->run())
			{
				$customer_report_contentgroup = Model_Customer_Report_Contentgroup::forge(array(
					'name' => Input::post('name'),
				));

				if ($customer_report_contentgroup and $customer_report_contentgroup->save())
				{
					Session::set_flash('success', 'Added customer_report_contentgroup #'.$customer_report_contentgroup->id.'.');

					Response::redirect('customer/report/contentgroup');
				}

				else
				{
					Session::set_flash('error', 'Could not save customer_report_contentgroup.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Customer_Report_Contentgroups";
		$this->template->content = View::forge('customer/report/contentgroup/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('customer/report/contentgroup');

		if ( ! $customer_report_contentgroup = Model_Customer_Report_Contentgroup::find($id))
		{
			Session::set_flash('error', 'Could not find customer_report_contentgroup #'.$id);
			Response::redirect('customer/report/contentgroup');
		}

		$val = Model_Customer_Report_Contentgroup::validate('edit');

		if ($val->run())
		{
			$customer_report_contentgroup->name = Input::post('name');

			if ($customer_report_contentgroup->save())
			{
				Session::set_flash('success', 'Updated customer_report_contentgroup #' . $id);

				Response::redirect('customer/report/contentgroup');
			}

			else
			{
				Session::set_flash('error', 'Could not update customer_report_contentgroup #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$customer_report_contentgroup->name = $val->validated('name');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('customer_report_contentgroup', $customer_report_contentgroup, false);
		}

		$this->template->title = "Customer_report_contentgroups";
		$this->template->content = View::forge('customer/report/contentgroup/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('customer/report/contentgroup');

		if ($customer_report_contentgroup = Model_Customer_Report_Contentgroup::find($id))
		{
			$customer_report_contentgroup->delete();

			Session::set_flash('success', 'Deleted customer_report_contentgroup #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete customer_report_contentgroup #'.$id);
		}

		Response::redirect('customer/report/contentgroup');

	}

}
