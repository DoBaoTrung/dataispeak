<?php
class Controller_Customer_Report_Type extends Controller_Home
{

	public function action_index()
	{
		$data['customer_report_types'] = Model_Customer_Report_Type::find('all');
		$this->template->title = "Customer_report_types";
		$this->template->content = View::forge('customer/report/type/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('customer/report/type');

		if ( ! $data['customer_report_type'] = Model_Customer_Report_Type::find($id))
		{
			Session::set_flash('error', 'Could not find customer_report_type #'.$id);
			Response::redirect('customer/report/type');
		}

		$this->template->title = "Customer_report_type";
		$this->template->content = View::forge('customer/report/type/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Customer_Report_Type::validate('create');

			if ($val->run())
			{
				$customer_report_type = Model_Customer_Report_Type::forge(array(
					'name' => Input::post('name'),
				));

				if ($customer_report_type and $customer_report_type->save())
				{
					Session::set_flash('success', 'Added customer_report_type #'.$customer_report_type->id.'.');

					Response::redirect('customer/report/type');
				}

				else
				{
					Session::set_flash('error', 'Could not save customer_report_type.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Customer_Report_Types";
		$this->template->content = View::forge('customer/report/type/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('customer/report/type');

		if ( ! $customer_report_type = Model_Customer_Report_Type::find($id))
		{
			Session::set_flash('error', 'Could not find customer_report_type #'.$id);
			Response::redirect('customer/report/type');
		}

		$val = Model_Customer_Report_Type::validate('edit');

		if ($val->run())
		{
			$customer_report_type->name = Input::post('name');

			if ($customer_report_type->save())
			{
				Session::set_flash('success', 'Updated customer_report_type #' . $id);

				Response::redirect('customer/report/type');
			}

			else
			{
				Session::set_flash('error', 'Could not update customer_report_type #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$customer_report_type->name = $val->validated('name');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('customer_report_type', $customer_report_type, false);
		}

		$this->template->title = "Customer_report_types";
		$this->template->content = View::forge('customer/report/type/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('customer/report/type');

		if ($customer_report_type = Model_Customer_Report_Type::find($id))
		{
			$customer_report_type->delete();

			Session::set_flash('success', 'Deleted customer_report_type #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete customer_report_type #'.$id);
		}

		Response::redirect('customer/report/type');

	}

}
