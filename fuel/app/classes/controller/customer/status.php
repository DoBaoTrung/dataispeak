<?php
class Controller_Customer_Status extends Controller_Home
{

	public function action_index()
	{
		$data['customer_statuses'] = Model_Customer_Status::find('all');
		$this->template->title = "Customer_statuses";
		$this->template->content = View::forge('customer/status/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('customer/status');

		if ( ! $data['customer_status'] = Model_Customer_Status::find($id))
		{
			Session::set_flash('error', 'Could not find customer_status #'.$id);
			Response::redirect('customer/status');
		}

		$this->template->title = "Customer_status";
		$this->template->content = View::forge('customer/status/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Customer_Status::validate('create');

			if ($val->run())
			{
				$customer_status = Model_Customer_Status::forge(array(
					'name' => Input::post('name'),
					'class' => Helper::slugify(Input::post('name')),
				));

				if ($customer_status and $customer_status->save())
				{
					Session::set_flash('success', 'Added customer_status #'.$customer_status->id.'.');

					Response::redirect('customer/status');
				}

				else
				{
					Session::set_flash('error', 'Could not save customer_status.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Customer_Statuses";
		$this->template->content = View::forge('customer/status/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('customer/status');

		if ( ! $customer_status = Model_Customer_Status::find($id))
		{
			Session::set_flash('error', 'Could not find customer_status #'.$id);
			Response::redirect('customer/status');
		}

		$val = Model_Customer_Status::validate('edit');

		if ($val->run())
		{
			$customer_status->name = Input::post('name');
			$customer_status->class = Helper::slugify(Input::post('name'));

			if ($customer_status->save())
			{
				Session::set_flash('success', 'Updated customer_status #' . $id);

				Response::redirect('customer/status');
			}

			else
			{
				Session::set_flash('error', 'Could not update customer_status #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$customer_status->name = $val->validated('name');
				$customer_status->class = $val->validated('class');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('customer_status', $customer_status, false);
		}

		$this->template->title = "Customer_statuses";
		$this->template->content = View::forge('customer/status/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('customer/status');

		if ($customer_status = Model_Customer_Status::find($id))
		{
			$customer_status->delete();

			Session::set_flash('success', 'Deleted customer_status #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete customer_status #'.$id);
		}

		Response::redirect('customer/status');

	}

}
