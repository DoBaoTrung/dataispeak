<?php 
class Controller_Secret_Update extends Controller_Home
{
	
	public function before(){
		parent::before();
	}

	public function action_index(){
		$this->template->content = View::forge('secret/update');

		// 24/06/0217
		if (Input::method()=='POST'){
			// Table customers

			if( ! DBUtil::field_exists('customers', array('name')))
		    {
		        \DBUtil::add_fields('customers', array(
		            'name' => array('constraint' => 255, 'type' => 'varchar', 'null'=> true),
		        ));
		        echo "Added field 'name' to table 'customers' ";
		    }

		    if( ! DBUtil::field_exists('customers', array('potential_points')))
		    {
		        \DBUtil::add_fields('customers', array(
		            'potential_points' => array('constraint' => 11, 'type' => 'int', 'null'=> true, 'default' => 0),
		        ));
		        echo "Added field 'potential_points' to table 'customers' ";
		    }

		    if( ! DBUtil::field_exists('customers', array('inputter')))
		    {
		        \DBUtil::add_fields('customers', array(
		            'inputter' => array('constraint' => 11, 'type' => 'int', 'null'=> true),
		        ));
		        echo "Added field 'inputter' to table 'customers' ";
		    }

		    if( ! DBUtil::field_exists('customers', array('status')))
		    {
		        \DBUtil::add_fields('customers', array(
		            'status' => array('constraint' => 11, 'type' => 'int', 'null'=> true, 'default' => 1),
		        ));
		        echo "Added field 'status' to table 'customers' ";
		    }

		    if( ! DBUtil::field_exists('customers', array('bonus_points')))
		    {	
		    	// Lưu điểm tự đánh giá vào một mảng json
		        \DBUtil::add_fields('customers', array(
		            'bonus_points' => array('constraint' => 255, 'type' => 'varchar', 'null'=> true),
		        ));
		        echo "Added field 'bonus_points' to table 'customers' ";
		    }

		    // 28/06/2017

		    if (! DBUtil::table_exists('customer_reports')){
		    	\DBUtil::create_table('customer_reports', array(
					'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
					'date' => array('constraint' => 11, 'type' => 'int', 'null' => true),
					'customer_name' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
					'type' => array('constraint' => 11, 'type' => 'int', 'null' => true),
					'content_group' => array('constraint' => 11, 'type' => 'int'),
					'content' => array('type' => 'text'),
					'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
					'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

				), array('id'));
		    }

		    if (! DBUtil::table_exists('customer_report_contentgroups')){
			    \DBUtil::create_table('customer_report_contentgroups', array(
					'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
					'name' => array('constraint' => 255, 'type' => 'varchar'),
					'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
					'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

				), array('id'));
			}

			if (! DBUtil::table_exists('customer_report_types')){
			    \DBUtil::create_table('customer_report_types', array(
					'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
					'name' => array('constraint' => 255, 'type' => 'varchar'),
					'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
					'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

				), array('id'));
			}
		}
	}
}
?>