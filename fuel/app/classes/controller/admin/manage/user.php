<?php

class Controller_Admin_Manage_User extends Controller_Admin
{
	const DEFAULT_GROUP = 1;

	public function action_index()
	{
		$this->template->set_global('users',Model_User::get_all_users());
		$this->template->title = 'Quản lý thành viên';
		$this->template->content = View::forge('admin/manage/user/index',array(),false);
	}

	public function action_create(){
		if (Input::method() == 'POST'){
			$username = str_replace(' ','',trim(Input::post('username')));
			$password = Input::post('password');
			$email = Input::post('email');
			$group = self::DEFAULT_GROUP;
			$profile_fields = Input::post('profile_fields');
			try {
				$result = Auth::create_user($username,$password,$email,$group,$profile_fields);
				if ($result){
					Session::set_flash('success','Tạo tài khoản thành công!');
					Response::redirect('admin/manage/user/index');
				}
			}
			catch (SimpleUserUpdateException $e) {
				Session::set_flash('notice',$e->getMessage());
			}	
		}
		$this->template->title = "Thêm thành viên";
		$this->template->content =  View::forge('admin/manage/user/create');
	}

	public function action_edit($id){
		if ($user = Model_User::get_one_user($id)){
			if (Input::method() == 'POST'){

				if (Input::post('reset_password')){
					$this->reset_password($id);
				}

				// Username to update
				$username = Model_User::query()->where('id','=',$id)->get_one()->username;

				// Data to update
				$data['email'] = Input::post('email');
				$data['group'] = self::DEFAULT_GROUP;
				$profile_fields = Input::post('profile_fields');
				foreach ( $profile_fields as $key => $value) {
					$data[$key] = $value;
				}

				try {
					$result = Auth::update_user($data, $username);
					if ($result){
						Session::set_flash('success','Sửa thành công!');
						Response::redirect('admin/manage/user/index');
					}
				}
				catch (SimpleUserUpdateException $e) {
					Session::set_flash('notice',$e->getMessage());
				}	
			}
			$this->template->set_global('user',$user);
			$this->template->title = "Sửa thành viên";
			$this->template->content =  View::forge('admin/manage/user/edit');
		}
		else {
			Session::set_flash('notice', 'Thành viên #'.$id.' không tồn tại.');
			Response::redirect(Router::get('user_manage'));
		}
	}

	public function action_get_job_description(){
		if (Input::is_ajax()){
			$user_id = Input::get('user_id');
			
			$user = Model_User::get_one_user($user_id,array('profile_fields'));

			if (empty($user->profile_fields['job_description'])) 
				$user->profile_fields['job_description'] = 'Chưa có mô tả công việc';
			if (empty($user->profile_fields['name'])) 
				$user->profile_fields['name'] = 'Anonymous';
			echo json_encode($user->to_array());
		}
		exit;
	}

	public function action_delete($id){
		$username = Model_User::get_one_user($id)->username;
		if (Auth::delete_user($username))
			Session::set_flash('success','Đã xoá tải khoản '.$username);
		else Session::set_flash('notice','Có lỗi xảy ra.');
		Response::redirect(Router::get('user_manage'));
	}

	private function reset_password($id){
		$username = Model_User::get_one_user($id)->username;
		if ($result = Auth::reset_password($username)){
			Session::set_flash('success', 'Đã reset! Mật khẩu mới là: ' . $result);
			Session::set_flash('isSticky',true);
			Response::redirect(Router::get('user_manage'));
		}
		else Session::set_flash('notice','Có lỗi xảy ra!');
	}
}
