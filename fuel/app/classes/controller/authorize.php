<?php 

class Controller_Authorize extends Controller_Base
{
	public function before(){
		parent::before();
	}

	public function action_login()
	{
		// Already logged in
		Auth::check() and Response::redirect();

		$val = Validation::forge();

		if (Input::method() == 'POST')
		{
			$val->add('email', 'Email or Username')
			    ->add_rule('required');
			$val->add('password', 'Password')
			    ->add_rule('required');

			if ($val->run())
			{
				if ( ! Auth::check())
				{
					if (Auth::login(Input::post('email'), Input::post('password')))
					{
						// assign the user id that lasted updated this record
						foreach (\Auth::verified() as $driver)
						{
							if (($id = $driver->get_user_id()) !== false)
							{
								// credentials ok, go right in
								$current_user = Model\Auth_User::find($id[1]);
								Session::set_flash('success', e('Welcome, '.$current_user->username));
								Response::redirect();
							}
						}
					}
					else
					{
						Session::set_flash('error', 'Sai tên đăng nhập hoặc mật khẩu!');
						Response::redirect(Router::get('login'));
					}
				}
				else
				{
					Session::set_flash('notice', 'Already logged in!');
				}
			}
		}

		return View::forge('home/login');
	}

	/**
	 * The logout action.
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_logout()
	{
		Auth::logout();
		Response::redirect();
	}

}
 ?>