<?php 
	/**
	* 
	*/
	class Controller_User extends Controller_Home
	{
		public function before(){
			parent::before();
		}

		public function action_index(){
			if (Input::method() == 'POST') {
				$success_msg = '';

				$user_data = array();
				$user_data['name'] = Input::post('name');
				$user_data['email'] = Input::post('email');
				if (Auth::update_user($user_data)){
					$success_msg .= '- Cập nhật thông tin cá nhân thành công!<br>';
					Session::set_flash('success','Cập nhật thông tin cá nhân thành công!');
				}
				if (Input::post('old_password')){
					if (Input::post('new_password') !== Input::post('confirm_password')){
						Session::set_flash('notice','Mật khẩu mới và mật khẩu xác nhận không khớp!');
						return Response::redirect(Router::get('canhan'));
					}

					$old_password = Input::post('old_password');
					$new_password = Input::post('new_password');

					$val = Model_User::validate_password('password');
					if ($val->run()){
						if (Auth::change_password($old_password, $new_password)){
							$success_msg .= '- Cập nhật mật khẩu thành công!<br>';
							Session::set_flash('success',$success_msg);
						}
						else Session::set_flash('error','Sai mật khẩu cũ!');
					}
					else {
						Session::set_flash('error',$val->error('new_password')->get_message('<b>Mật khẩu mới không hợp lệ!</b><br>- Mật khẩu phải ít nhất 8 ký tự<br>- Không kí tự đặc biệt hoặc tiếng việt có dấu!'));
					}
				}
			}
			$this->template->title = 'Trang cá nhân';
			$this->template->content = View::forge('home/user');
		}
	}
 ?>