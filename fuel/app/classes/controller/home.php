<?php

class Controller_Home extends Controller_Base
{

	public $template = 'template';

	public function before(){
		parent::before();
		if (!Auth::check())
		{
			Response::redirect(Router::get('login'));
		}
		date_default_timezone_set('Asia/Bangkok');
	}

	public function action_index()
	{	
		$this->template->title = 'Trang chủ';
		$this->template->content = View::forge('home/index');
	}
}
