<?php 

class Helper
{

	public static function slugify($str){
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s.]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        return $str;
	}

	public static function pr($data){
		echo "<pre>";
		var_dump($data);
		echo "</pre>";
		exit;
	}

	public static function print_current_date_vi(){
		echo date('j \t\h\á\n\g n \n\ă\m Y',time());
	}

	public static function print_gioitinh($data){
		switch ($data) {
			case '1':
				echo "Nam";
				break;
			
			default:
				echo "Nữ";
				break;
		}
	}
	
	public static function print_roi_chua($data){
		switch ($data) {
			case '1':
				echo "Rồi";
				break;
			
			default:
				echo "Chưa";
				break;
		}
	}

	public static function print_co_khong($data){
		switch ($data) {
			case '1':
				echo "Có";
				break;
			
			default:
				echo "Không";
				break;
		}
	}

	public static function print_ketquahoctap($data){
		switch ($data) {
			case '3':
				echo "Xuất sắc";
				break;
			case '2':
				echo "Giỏi";
				break;
			case '1':
				echo "Khá";
				break;
			default:
				echo "Trung bình";
				break;
		}
	}

	public static function print_tansuat($data){
		switch ($data) {
			case '2':
				echo "Thường xuyên";
				break;
			case '1':
				echo "Thỉnh thoảng";
				break;
			default:
				echo "Không";
				break;
		}
	}

	public static function print_internet($data){
		switch ($data) {
			case '1':
				echo "Mạng tốt";
				break;
			default:
				echo "Mạng miền núi";
				break;
		}
	}
}
?>