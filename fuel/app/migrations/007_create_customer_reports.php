<?php

namespace Fuel\Migrations;

class Create_customer_reports
{
	public function up()
	{
		\DBUtil::create_table('customer_reports', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'date' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'customer_name' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'type' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'content_group' => array('constraint' => 11, 'type' => 'int'),
			'content' => array('type' => 'text'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('customer_reports');
	}
}