<?php

namespace Fuel\Migrations;

class Create_customer_info_learningenglish
{
	public function up()
	{
		\DBUtil::create_table('customer_info_learningenglish', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'customer_id' => array('constraint' => 11, 'type' => 'int'),
			'education_center' => array('constraint' => 11, 'type' => 'int'),
			'online_learning' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'education_center_name' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'classes_per_week' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'parents_pick_up' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'learning_with_foreign_teachers' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('customer_info_learningenglish');
	}
}