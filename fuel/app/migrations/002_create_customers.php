<?php

namespace Fuel\Migrations;

class Create_customers
{
	public function up()
	{
		\DBUtil::create_table('customers', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'fullname' => array('constraint' => 255, 'type' => 'varchar'),
			'sex' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'age' => array('constraint' => 11, 'type' => 'int'),
			'address' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'school_name' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'study_result' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('customers');
	}
}