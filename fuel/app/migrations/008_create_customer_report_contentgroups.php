<?php

namespace Fuel\Migrations;

class Create_customer_report_contentgroups
{
	public function up()
	{
		\DBUtil::create_table('customer_report_contentgroups', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name' => array('constraint' => 255, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('customer_report_contentgroups');
	}
}