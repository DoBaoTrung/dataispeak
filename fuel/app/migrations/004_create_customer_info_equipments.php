<?php

namespace Fuel\Migrations;

class Create_customer_info_equipments
{
	public function up()
	{
		\DBUtil::create_table('customer_info_equipments', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'customer_id' => array('constraint' => 11, 'type' => 'int'),
			'laptop' => array('constraint' => 11, 'type' => 'int'),
			'pc' => array('constraint' => 11, 'type' => 'int'),
			'headphone' => array('constraint' => 11, 'type' => 'int'),
			'micro' => array('constraint' => 11, 'type' => 'int'),
			'internet' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('customer_info_equipments');
	}
}