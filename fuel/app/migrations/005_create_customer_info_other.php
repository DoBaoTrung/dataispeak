<?php

namespace Fuel\Migrations;

class Create_customer_info_other
{
	public function up()
	{
		\DBUtil::create_table('customer_info_other', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'customer_id' => array('constraint' => 11, 'type' => 'int'),
			'study_with_parents' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'parents_job' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('customer_info_other');
	}
}